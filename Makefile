all: configure build deploy

configure:
	go get -u github.com/rusart/sup/cmd/sup

build:
	go build ./

deploy:
	sup -D prod deploy
