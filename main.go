package main

import (
	"crypto/md5"
	"encoding/hex"
	"encoding/json"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"github.com/go-telegram-bot-api/telegram-bot-api"
	"github.com/golang-collections/collections/stack"
	"github.com/jinzhu/gorm"
	"github.com/labstack/gommon/log"
	"github.com/spf13/viper"
	"io/ioutil"
	"net/http"
	"os"
	"strings"
	"time"
)

var (
	DB  *gorm.DB
	bot *tgbotapi.BotAPI
	err error
)

type Item struct {
	Id    uint              `json:"id"`
	Title map[string]string `json:"title"`
	Slug  string            `json:"slug"`
	Img   string            `json:"img"`
}

type Response struct {
	Total uint   `json:"total"`
	Items []Item `json:"items"`
}

func main() {
	readConfig()

	connectAndCheck()
	defer DB.Close()

	migrate()

	bot, err = tgbotapi.NewBotAPI(viper.GetString("bot.token"))
	if err != nil {
		log.Fatal(err)
	}

	bot.Debug = true

	log.Printf("Authorized on account %s", bot.Self.UserName)

	exit := make(chan interface{})

	// Обработчик входящих сообщений боту
	go func() {
		defer close(exit)
		u := tgbotapi.NewUpdate(0)
		u.Timeout = 60

		updates, _ := bot.GetUpdatesChan(u)

		log.Info("Getting tg updates started")

		for update := range updates {
			if update.Message == nil {
				continue
			}

			log.Printf("[%s] %s", update.Message.From.UserName, update.Message.Text)
			if update.Message != nil {
				msg := tgbotapi.NewMessage(update.Message.Chat.ID, "")
				msg.ReplyToMessageID = update.Message.MessageID
				msg.ParseMode = "markdown"
				if update.Message.IsCommand() {
					switch update.Message.Command() {
					case "help":
						msg.Text = "Никто тебе не поможет кроме тебя самого"
					case "start":
						registerSubscriber(update.Message)
						msg.Text = "Привет. Теперь вы будете получать оповещения от меня при появлении новых новостей на сайте https://infinitythegame.com/blog/"
					case "last":
						sendLast(update.Message)
					default:
						msg.Text = "Во многих знаниях многие печали"
					}
				}
				bot.Send(msg)
			}
		}
	}()

	// Получение новых данных со страницы блога
	go func() {
		defer close(exit)

		for {
			var subs []Subscriber
			DB.Find(&subs)

			if response, err := http.Get("https://api.corvusbelli.com/web/news/infinity/new"); err == nil {
				if response.StatusCode == http.StatusOK {
					var resp Response
					if content, err := ioutil.ReadAll(response.Body); err == nil {

						err := json.Unmarshal(content, &resp)
						if err == nil {
							var news stack.Stack
							for _, item := range resp.Items {
								itemHash := fmt.Sprintf("%d", item.Id)
								var sentNews News
								if DB.Where(News{Hash: itemHash}).First(&sentNews).RecordNotFound() {
									news.Push(item)
								} else {
									break
								}
							}

							if news.Len() > 0 {
								max := news.Len()

								for i := 0; i < max; i++ {
									item := news.Pop().(Item)
									itemHash := fmt.Sprintf("%d", item.Id)
									jsonItem, _ := json.Marshal(item)
									news := News{
										Hash:      itemHash,
										Text:      string(jsonItem),
										CreatedAt: time.Now(),
									}

									err = DB.Save(news).Error
									if err != nil {
										log.Errorf("Error saving news: %s", err.Error())
									}

									newsText := prepareItem(item)

									sendToAll(subs, newsText)
								}
							}
						} else {
							log.Warnf("Error decode body: %s", err.Error())
						}
					} else {
						log.Warnf("Error read body: %s", err.Error())
					}

				} else {
					log.Warnf("Error status code: %v", response.StatusCode)
				}
				response.Body.Close()
			} else {
				log.Warnf("Error getting page: %s", err.Error())
			}

			if botNews, err := ioutil.ReadFile("./botnews"); err == nil && strings.Trim(string(botNews), " \n") != "" {
				sendToAll(subs, string(botNews))
				if err := os.Remove("./botnews"); err != nil {
					log.Errorf("Error deleting botnews file: %s", err.Error())
				}
			}

			time.Sleep(time.Minute * 5)
		}
	}()

	<-exit
}

func sendToAll(subs []Subscriber, text string) {
	for _, subscriber := range subs {
		msg := tgbotapi.NewMessage(int64(subscriber.ID), "")
		msg.ParseMode = "html"
		msg.Text = text
		bot.Send(msg)
	}
}

func registerSubscriber(msg *tgbotapi.Message) {
	sub := Subscriber{
		ID:           msg.From.ID,
		FirstName:    msg.From.FirstName,
		LastName:     msg.From.LastName,
		Username:     msg.From.UserName,
		LanguageCode: msg.From.LanguageCode,
	}

	DB.FirstOrCreate(&sub)
}

func sendLast(inMsg *tgbotapi.Message) {
	sub := Subscriber{
		ID: inMsg.From.ID,
	}

	var lastNews News
	DB.Order("created_at DESC").First(&lastNews)
	var item Item
	err := json.Unmarshal([]byte(lastNews.Text), &item)
	var newsText string
	if err == nil {
		newsText = prepareItem(item)
	} else {
		newsText = "Что-то поломалось"
	}

	DB.Where(sub).First(&sub)

	msg := tgbotapi.NewMessage(int64(sub.ID), "")
	msg.ReplyToMessageID = inMsg.MessageID
	msg.ParseMode = "html"
	msg.Text = newsText
	bot.Send(msg)
}

func readConfig() {
	viper.SetConfigType("yaml")
	viper.SetConfigName("config")
	viper.AddConfigPath(".")
	err := viper.ReadInConfig()
	if err != nil {
		log.Fatalf("Fatal error config file: %s \n", err)
	}
}

func connectAndCheck() {
	var err error
	DB, err = gorm.Open("mysql", viper.GetString("mysql.conn"))
	if err != nil {
		log.Fatalf("ошибка подключения к базе данных", err.Error())
	}

	DB.DB().SetMaxIdleConns(10)
	DB.DB().SetMaxOpenConns(50)

	//DB = DB.Debug()
}

func migrate() {
	DB.AutoMigrate(
		&Subscriber{},
		&News{},
	)
}

func GetMD5Hash(text string) string {
	hasher := md5.New()
	hasher.Write([]byte(text))
	return hex.EncodeToString(hasher.Sum(nil))
}

func prepareItem(item Item) string {
	text := fmt.Sprintf("<a href=\"%s\">⁣</a><b>%s</b>\nhttps://www.infinitythegame.com/blog/item/%d-%s", item.Img, item.Title["en"], item.Id, item.Slug)
	return text
}
